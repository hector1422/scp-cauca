const functions = require('firebase-functions');
var admin = require("firebase-admin");
var serviceAccount = require("./scp-cauca-firebase.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://scp-cauca.firebaseio.com"
});

exports.enviarCorreo= functions.firestore.document("email/{id}")
.onCreate(async(snapshot, context) => {
    const itemDataSnap = await snapshot.ref.get()
    return admin.firestore().collection('mail').add({
       to: [itemDataSnap.data().to],
       message: {
         subject: ' Correo de Contacto SCP-Cauca',
         html: '<img style="height: 1cm; width: 1cm;" src="https://firebasestorage.googleapis.com/v0/b/scp-cauca.appspot.com/o/images%2Flogo-scpcauca.png?alt=media&token=c624dd51-5822-4cb2-a742-df40d792c60b" alt=""> <br><br>Remitente: <br> Nombre:  '+ itemDataSnap.data().nombre +'<br>     Correo:  ' + itemDataSnap.data().emailAdress +'<br>Asunto: '+itemDataSnap.data().subject+'<br>Mensaje: '+itemDataSnap.data().message
       }
     }).then(() => console.log('Email en Cola!'));
});

                     
                      