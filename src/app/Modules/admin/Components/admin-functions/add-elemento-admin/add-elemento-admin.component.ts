import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { ElementsAdminService } from '../../../Services/elements-admin.service';
import { Element } from '../../../../../Model/element.model'
import Swal from 'sweetalert2'
import { firestore } from 'firebase/app';
import Timestamp = firestore.Timestamp;
/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
@Component({
  selector: 'app-add-elemento-admin',
  templateUrl: './add-elemento-admin.component.html',
  styleUrls: ['./add-elemento-admin.component.scss']
})
export class AddElementoAdminComponent implements OnInit {

  tipoFormGroup: FormGroup;
  tituloFormGroup: FormGroup;
  resumenFormGroup: FormGroup;
  descripcion_largaFormGroup: FormGroup;
  fechaFormGroup: FormGroup;
  imagenFormGroup: FormGroup;
  source: string = '';
  tipos: string[] = ['Noticias', 'Eventos', 'Artículos', 'Padres', 'Videos'];
  matcher = new MyErrorStateMatcher();
  image: any;
  element: Element;
  bandImg: boolean;
  video: string = '';
  bandVideo: boolean;
  constructor(private _formBuilder: FormBuilder, private elementService: ElementsAdminService) { }

  ngOnInit() {
    this.bandImg = false;
    this.element = new Element();
    this.tipoFormGroup = this._formBuilder.group({
      tipo: ['', Validators.required]
    });
    this.tipoFormGroup.get('tipo').valueChanges.subscribe(
      value => { this.element.tipo = value; }
    );
    this.tituloFormGroup = this._formBuilder.group({
      titulo: ['', Validators.required]
    });
    this.tituloFormGroup.get('titulo').valueChanges.subscribe(
      value => { this.element.titulo = value; }
    );
    this.resumenFormGroup = this._formBuilder.group({
      resumen: ['', Validators.required]
    });
    this.resumenFormGroup.get('resumen').valueChanges.subscribe(
      value => { this.element.resumen = value; }
    );
    this.descripcion_largaFormGroup = this._formBuilder.group({
      descripcion_larga: ['', Validators.required]
    });
    this.descripcion_largaFormGroup.get('descripcion_larga').valueChanges.subscribe(
      value => { this.element.descripcion_larga = value; }
    );
    this.imagenFormGroup = this._formBuilder.group({
      imagen: ['', Validators.required]
    });
    this.imagenFormGroup.get('imagen').valueChanges.subscribe(
      value => { this.element.imagen = value; }
    );
  }
  openInput() {
    document.getElementById("fileInput").click();
  }
  errores(): boolean {
    return (this.tipoFormGroup.get('tipo').hasError('required') &&
      this.tituloFormGroup.get('titulo').hasError('required') &&
      this.resumenFormGroup.get('resumen').hasError('required') &&
      this.descripcion_largaFormGroup.get('descripcion_larga').hasError('required')
    ) ? true : false;

  }

  onUpload() {

    /*  element.titulo=this.tituloFormGroup.get('titulo').value();
     element.tipo=this.tipoFormGroup.get('tipo').value();
     element.resumen=this.resumenFormGroup.get('resumen').value();
     element.imagen=this.image;
     element.fecha=this.fechaFormGroup.get('fecha').value();
     element.descripcion_larga=this.descripcion_largaFormGroup.get('descripcion_larga').value(); */

    this.element.fecha = Timestamp.now();
    this.elementService.uploadElement(this.image, this.element);
    this.okMessage();

  }
  handleImage(event: any): void {
    this.projectImage(event.target.files[0]); 
  }
  projectImage(file: File) {

    if (file.type.slice(0, 5) == "image") {
      this.image = file;
      this.bandImg = true;
      let reader = new FileReader;
      // TODO: Define type of 'e'
      reader.onload = (e: any) => {
        // Simply set e.target.result as our <img> src in the layout
        this.source = e.target.result;

      };
      reader.readAsDataURL(file);
    }
    else this.typeErrorMessage();
    // This will process our file and get it's attributes/data
    

  }
  okMessage() {
    Swal.fire({
      icon: 'success',
      title: 'Elemento publicado',
      text: 'El elemento ha sido publicado!',
    });
    this.ngOnInit();
  }
  typeErrorMessage() {
    Swal.fire({
      icon: 'error',
      title: 'Tipo no Permitido',
      text: 'El tipo de archivo seleccionado no es permitido!',
    });
    this.ngOnInit();
  }

}
