 import {  firestore } from 'firebase/app';
 import Timestamp = firestore.Timestamp;
 export class Element {
     tipo:string;
     titulo:string;
     resumen:string;
     descripcion_larga:string;
     fecha:Timestamp;
     imagen:string;
     constructor()
     {
 
     }
   }