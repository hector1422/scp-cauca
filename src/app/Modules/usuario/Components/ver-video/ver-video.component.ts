import { Element } from '../../../../Model/element.model';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-ver-video',
  templateUrl: './ver-video.component.html',
  styleUrls: ['./ver-video.component.scss']
})
export class VerVideoComponent implements OnInit {
  noticia:Element;
  constructor(public dialogoReg:MatDialogRef<VerVideoComponent>) 
  { 
    this.noticia=new Element();
  }

  ngOnInit() {
  }
  ok()
  {
    this.dialogoReg.close();
  }


}
