import { Element } from '../../../../Model/element.model';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import {ThemePalette} from '@angular/material/core';
import { ElementUsuarioService } from '../../Services/element-usuario.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  noticias:Element[];

  constructor( config: NgbCarouselConfig, private elementService:ElementUsuarioService) { 
    // customize default values of carousels used by this component tree
    this.noticias= [];
    config.interval = 5000;
    config.wrap = true;
    config.keyboard = false;
    config.pauseOnHover = false;
  }

  ngOnInit(): void {
    this.obtenerElementos();
  }
  obtenerElementos()
  {
    this.elementService.getElementos().subscribe(result=>{
      result.forEach(e=>
        {
          if(e.tipo=="Noticias")
          {
            
            let elm= new Element;
            elm.titulo=e.titulo;
            elm.descripcion_larga=e.descripcion_larga;
            elm.fecha=e.fecha;
            elm.imagen=e.imagen;
            elm.resumen=e.resumen;
            elm.tipo=e.tipo;            
            
            this.noticias.push(elm);
          }
        }
       
        );
    });
  }


}
