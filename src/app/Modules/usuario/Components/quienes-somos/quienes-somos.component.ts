import { ElementsService } from 'src/app/Services/elements.service';
import { QuienesSomos } from '../../../../Model/quienes-somos.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-quienes-somos',
  templateUrl: './quienes-somos.component.html',
  styleUrls: ['./quienes-somos.component.scss']
})
export class QuienesSomosComponent implements OnInit {
  qs:QuienesSomos[];
  constructor(private elementService:ElementsService) 
  { 
    this.qs=[];
  }

  ngOnInit(): void {
    this.obtenerElementos();
  }
  obtenerElementos()
  {
    this.elementService.getQuienesSomos().subscribe(result=>{
      result.forEach(e=>
        {
            let elm= new QuienesSomos();
            elm.historia=e.historia;
            elm.mision=e.mision;
            elm.vision=e.vision;
            this.qs.push(elm);
        }
        );
    });
  }

}
