import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, finalize } from 'rxjs/operators';
const SERVER_EMAIL_ROUTE: string = 'http://localhost:3000';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AngularFireList, AngularFireDatabase } from '@angular/fire/database';
import {Email} from '../../../Model/email.model'
import {
  AngularFirestore,
  AngularFirestoreCollection,
  DocumentReference,
} from '@angular/fire/firestore';

import { AngularFireStorage } from '@angular/fire/storage';

const COLECCION_EMAIL: string = '/email';
@Injectable({
  providedIn: 'root'
})
export class EmailService {
  private postsCollection: AngularFirestoreCollection<Email>;
  private filePath: any;
  private downloadURL: Observable<string>;
  private emailDB: AngularFireList<Email>;
  private todoCollectionName = 'email';
constructor( private afs: AngularFirestore){

}

 
  addEmail(email:Email): Promise<DocumentReference> {
    return this.afs.collection(this.todoCollectionName).add(email);
   }


/* sendMessage(body) {
  return this.httpClient.post('http://localhost:3000/formulario', body);
  } */


}
