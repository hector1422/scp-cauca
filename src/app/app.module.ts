import { PediatrasService } from './Services/pediatras.service';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//MODULO ADMIN
import {AdminModule} from './Modules/admin/admin.module'
import {AdminRoutes} from './Modules/admin/admin.routing'
//MODULO USUARIO
import {UsuarioModule} from './Modules/usuario/usuario.module';
import {UsuarioRoutes} from './Modules/usuario/usuario.routing';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


import {environment} from '../environments/environment';
import {AngularFireDatabaseModule} from '@angular/fire/database'
/* Firebase */
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';



//ANGULAR MATERIAL

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatStepperModule} from '@angular/material/stepper';
import {MatSelectModule} from '@angular/material/select';


import {MatNativeDateModule} from '@angular/material/core';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import {MatTableModule} from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import {MatPaginatorIntl} from '@angular/material/paginator';


import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import {YouTubePlayerModule} from '@angular/youtube-player';




@NgModule({
  declarations: [
    AppComponent,


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatStepperModule,
    MatSelectModule,
    MatNativeDateModule,
    MatCardModule,

    MatDialogModule,
    MatTableModule,
    MatPaginatorModule,

    MatSortModule,
    AdminModule,

    AdminRoutes,
    UsuarioModule,
    UsuarioRoutes,
    YouTubePlayerModule



   /*  AgmCoreModule.forRoot({
      apiKey: 'TU_API_KEY'
   }), */

  ],
  providers: [{
    provide: LocationStrategy,
    useClass: HashLocationStrategy,
  },



  {provide: MatDialogRef, useValue: {}}, PediatrasService
],
  bootstrap: [AppComponent]
})
export class AppModule { }
