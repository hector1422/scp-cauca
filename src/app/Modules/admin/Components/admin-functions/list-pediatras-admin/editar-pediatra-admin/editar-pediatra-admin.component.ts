import { PediatraAdminService } from '../../../../Services/pediatra-admin.service';
import { Component, OnInit } from '@angular/core';
import { Pediatra } from '../../../../../../Model/pediatra.model';

import { MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-editar-pediatra-admin',
  templateUrl: './editar-pediatra-admin.component.html',
  styleUrls: ['./editar-pediatra-admin.component.scss']
})
export class EditarPediatraAdminComponent implements OnInit {
  pediatra:Pediatra;
  pediatraRef:any;
  nuevoPediatra:Pediatra;
  formulario: FormGroup;
  image: any;
  imageBandera:boolean;
  source:string='';
  constructor(public dialogoReg: MatDialogRef<EditarPediatraAdminComponent>, 
    private formBuilder: FormBuilder, private pediatraService:PediatraAdminService) { }

  ngOnInit() {
    this.imageBandera = false;
    this.crearFormulario();
  }
  crearFormulario() {
    this.formulario = this.formBuilder.group(
      {
        nombre: [this.pediatraRef.nombre, [Validators.required,Validators.pattern('^[A-Za-z-ñÑáéíóúÁÉÍÓÚ ]+[a-zA-Z\\s]*')]],
        profesion: [this.pediatraRef.profesion, [Validators.required,Validators.pattern('^[A-Za-z-ñÑáéíóúÁÉÍÓÚ ]+[a-zA-Z\\s]*')]],
        descripcion: [this.pediatraRef.descripcion, [Validators.required]]       
      });
    this.formulario.valueChanges.subscribe(
      value => {
        /*  console.log("Otro valor Contacto: ", value); */
      }
    );
  }
  onSubmit() {
    this.nuevoPediatra = new Pediatra();
    this.nuevoPediatra.nombre = this.formulario.get('nombre').value;
    this.nuevoPediatra.profesion = this.formulario.get('profesion').value;
    this.nuevoPediatra.descripcion = this.formulario.get('descripcion').value;

    if (this.imageBandera) {
      this.pediatraService.editElementImage(this.image, this.pediatraRef, this.nuevoPediatra);
    }

    else {
      this.nuevoPediatra.imagen = this.pediatraRef.imagen;
      this.pediatraService.editPediatra(this.pediatraRef, this.nuevoPediatra);
    }
    this.okMessage();
    this.dialogoReg.close();
  }
  handleImage(event: any): void {
    this.image = event.target.files[0];
    this.imageBandera = true;
    this.projectImage(event.target.files[0]);
  }
  errores(): boolean {
    return (this.formulario.get('nombre').hasError('required') ||
    this.formulario.get('nombre').hasError('pattern') ||
    this.formulario.get('profesion').hasError('required') ||
    this.formulario.get('profesion').hasError('pattern') ||
      this.formulario.get('descripcion').hasError('required') 
      ) ? true : false;

  }
  projectImage(file: File) {
    let reader = new FileReader;
    // TODO: Define type of 'e'
    reader.onload = (e: any) => {
        // Simply set e.target.result as our <img> src in the layout
        this.source = e.target.result;
        
    };
    // This will process our file and get it's attributes/data
    reader.readAsDataURL(file);
}
okMessage() {
  Swal.fire({
    icon: 'success',
    title: 'Pediatra Editado',
    text: 'El Pediatra fué editado Correctamente!',
  });
  this.ngOnInit();
}
cancel() {
  Swal.fire({
    icon: 'error',
    title: 'Pediatra NO Editado',
    text: 'El Pediatra NO fué editado!',
  });
  this.dialogoReg.close();
}

}
