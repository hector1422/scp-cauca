import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Element } from '../../../../Model/element.model';
import { ElementUsuarioService } from '../../Services/element-usuario.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { VerVideoComponent } from '../ver-video/ver-video.component';

const NUMERO_NOTICIAS_RECIENTES=3;
@Component({
  selector: 'app-categorias-video',
  templateUrl: './categorias-video.component.html',
  styleUrls: ['./categorias-video.component.scss']
})
export class CategoriasVideoComponent implements OnInit {
  panelOpenState = false;
  noticias:Element[];
  @Input() tipo:string;
  constructor(private elementService:ElementUsuarioService, private router: Router,
    private dialog: MatDialog) 
  { 
    this.noticias= [];
    this.tipo="Noticias";
  }

  ngOnInit(): void {
    this.obtenerElementos();
    this.tipo=this.router.url;
    if(this.tipo=="/padres")
    {
      
      this.tipo="Para padres"
    }
    const tag = document.createElement('script');
    tag.src = 'https://www.youtube.com/iframe_api/';
    document.body.appendChild(tag);
  }
  obtenerElementos()
  {
    
    this.elementService.getElementos().subscribe(result=>{
      result.forEach(e=>
        { 
          
          if(('/'+e.tipo.toLowerCase())==this.router.url)
          {
            let elm= new Element();
            elm.titulo=e.titulo;
            elm.descripcion_larga=e.descripcion_larga;
            elm.fecha=e.fecha;
            elm.imagen=e.imagen;
            elm.resumen=e.resumen;
            elm.tipo=e.tipo;
            if(this.noticias.length<NUMERO_NOTICIAS_RECIENTES)
              {this.noticias.push(elm);
                this.noticias.sort(this.ordenar);
              }
          }
        }
       
        );
    });
    
  }
  ordenar(a,b){   
    return a.fecha.seconds-b.fecha.seconds;
  }
ngDoCheck(): void {
 this.tipo=this.capitalize(this.router.url.slice(1));
}
verNoticia(n:Element)
  {
    console.log("VER");
    const dialogRef = this.dialog.open(VerVideoComponent, {
      width: '800px',
      height:'500px',
      data:{}
    });
   dialogRef.componentInstance.noticia=n; 
    dialogRef.afterClosed().subscribe(result => {/* 
      console.log(`Dialog result: ${result}`); */
    });
  }
capitalize(word:string) {
  return word[0].toUpperCase() + word.slice(1);
}

}
