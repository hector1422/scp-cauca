import { HomeComponent } from './Components/home/home.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {VerPediatraUsuarioComponent} from './Components/ver-pediatra-usuario/ver-pediatra-usuario.component';
import {ListPediatrasComponent} from './Components/list-pediatras/list-pediatras.component'
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatStepperModule } from '@angular/material/stepper';
import { MatSelectModule } from '@angular/material/select';
import { MatNativeDateModule } from '@angular/material/core';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import {ListNoticiasComponent} from './Components/list-noticias/list-noticias.component'
import {VerNoticiaComponent} from './Components/ver-noticia/ver-noticia.component' 
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {ContactoComponent} from './Components/contacto/contacto.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {ListArticulosComponent} from './Components/list-articulos/list-articulos.component'
import {ListParaPadresComponent} from './Components/list-para-padres/list-para-padres.component'
import {ListEventosComponent} from './Components/list-eventos/list-eventos.component'
import {QuienesSomosComponent} from './Components/quienes-somos/quienes-somos.component';
import {CategoriasUsuarioComponent} from './Components/categorias-usuario/categorias-usuario.component'
import {MatExpansionModule} from '@angular/material/expansion';
import {HttpClientModule} from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';
import {UbicacionUsuarioComponent} from './Components/ubicacion-usuario/ubicacion-usuario.component'
import { VideosComponent } from './Components/videos/videos.component';
import {YouTubePlayerModule} from '@angular/youtube-player';
import { CategoriasVideoComponent } from './Components/categorias-video/categorias-video.component';
import { VerVideoComponent } from './Components/ver-video/ver-video.component';
@NgModule({
  
  declarations: [VerPediatraUsuarioComponent,
    ListPediatrasComponent,
    ListNoticiasComponent,
    VerNoticiaComponent,
    HomeComponent,
    ContactoComponent,
    ListArticulosComponent,
    QuienesSomosComponent,
    ListParaPadresComponent,
    ListEventosComponent,
    CategoriasUsuarioComponent,
    UbicacionUsuarioComponent,
    VideosComponent,
    CategoriasVideoComponent,
    VerVideoComponent
  ],
    


    imports: [
    CommonModule,
    MatDatepickerModule,
    MatInputModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatStepperModule,
    MatSelectModule,
    MatNativeDateModule,
    MatCardModule,
    MatDialogModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    MatExpansionModule,
    HttpClientModule,
    YouTubePlayerModule,
    AgmCoreModule.forRoot({
         apiKey: 'AIzaSyDy5XQozuVP2-aGe1rdGTg7GHnArm5jVVc'
      // apiKey: 'AIzaSyDVHlGp6mCOotZiOkevL9O9Rma8tRdLQBU'  
   })
  ],
  providers: [
    { provide: MatDialogRef, useValue: {} }]
})
export class UsuarioModule { }
