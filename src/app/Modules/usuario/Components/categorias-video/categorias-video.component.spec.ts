import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriasVideoComponent } from './categorias-video.component';

describe('CategoriasVideoComponent', () => {
  let component: CategoriasVideoComponent;
  let fixture: ComponentFixture<CategoriasVideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoriasVideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriasVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
