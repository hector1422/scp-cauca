import { Element } from '../../../../Model/element.model';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
@Component({
  selector: 'app-ver-noticia',
  templateUrl: './ver-noticia.component.html',
  styleUrls: ['./ver-noticia.component.scss']
})
export class VerNoticiaComponent implements OnInit {
  noticia:Element;
  constructor(public dialogoReg:MatDialogRef<VerNoticiaComponent>) 
  { 
    this.noticia=new Element();
  }

  ngOnInit() {
  }
  ok()
  {
    this.dialogoReg.close();
  }

}
