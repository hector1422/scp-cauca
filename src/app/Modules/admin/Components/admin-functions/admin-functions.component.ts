import { ElementsAdminService } from '../../Services/elements-admin.service';
import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, ReactiveFormsModule, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import {ThemePalette} from '@angular/material/core';
import { AuthService } from '../../Services/auth.service';


@Component({
  selector: 'app-admin-functions',
  templateUrl: './admin-functions.component.html',
  styleUrls: ['./admin-functions.component.scss']
})
export class AdminFunctionsComponent implements OnInit {
  log=false;
  pediatra=false;
  editar=false;
  publicar=false;
  ver=true;
  qs=false;
  constructor(private elementService:ElementsAdminService, private authService:AuthService) { }

  ngOnInit() {
    this.log=this.authService.isLoggedIn;
  }
  cerrarSesion()
  {
    this.authService.SignOut();
  }
  onPublic()
  {
    this.publicar=true;
    this.editar=false;
    this.ver=false;
    this.qs=false;
    this.pediatra=false;
  }
  onPediatra()
  {
    this.publicar=false;
    this.editar=false;
    this.ver=false;
    this.qs=false;
    this.pediatra=true;
  }
  onQs()
  {
    this.publicar=false;
    this.editar=false;
    this.ver=false;
    this.qs=true;
    this.pediatra=false;
  }
  onEdit()
  {
    this.publicar=false;
    this.editar=true;
    this.ver=false;
    this.qs=false;
    this.pediatra=false;
  }
  onVer()
  {
    this.publicar=false;
    this.editar=false;
    this.ver=true;
    this.qs=false;
    this.pediatra=false;
  }
}
