import { Pediatra } from '../../../../../../Model/pediatra.model';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-ver-pediatra-admin',
  templateUrl: './ver-pediatra-admin.component.html',
  styleUrls: ['./ver-pediatra-admin.component.scss']
})
export class VerPediatraAdminComponent implements OnInit {
  p:Pediatra;
  constructor(public dialogoReg:MatDialogRef<VerPediatraAdminComponent>) 
  {
    this.p=new Pediatra();
  }

  ngOnInit() {
  }
  ok()
  {
    this.dialogoReg.close();
  }

}
