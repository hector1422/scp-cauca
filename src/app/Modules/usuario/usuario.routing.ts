import { QuienesSomosComponent } from './Components/quienes-somos/quienes-somos.component';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './Components/home/home.component';
import { ListPediatrasComponent } from './Components/list-pediatras/list-pediatras.component';
import { ListNoticiasComponent } from './Components/list-noticias/list-noticias.component';
import { ListEventosComponent } from './Components/list-eventos/list-eventos.component';
import { ListParaPadresComponent } from './Components/list-para-padres/list-para-padres.component';
import { ListArticulosComponent } from './Components/list-articulos/list-articulos.component';
import { ContactoComponent } from './Components/contacto/contacto.component';
import { VideosComponent } from './Components/videos/videos.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },{
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'quienes-somos',
    component: QuienesSomosComponent,
  },
  {
    path: 'pediatras',
    component: ListPediatrasComponent,
  },
  {
    path: 'noticias',
    component: ListNoticiasComponent,
   
  },
  {
    path: 'eventos',
    component: ListArticulosComponent,
   
  },
  {
    path: 'padres',
    component: ListNoticiasComponent,
   
  },
  {
    path: 'articulos',
    component: ListArticulosComponent,
   
  },
  {
    path: 'contactanos',
    component: ContactoComponent,
    
  },
  {
    path: 'videos',
    component: VideosComponent,
  }
];

export const UsuarioRoutes = RouterModule.forChild(routes);
