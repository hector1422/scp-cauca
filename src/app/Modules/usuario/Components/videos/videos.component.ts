import { Element } from '../../../../Model/element.model';
import { ElementUsuarioService } from '../../Services/element-usuario.service';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { VerNoticiaComponent } from '../ver-noticia/ver-noticia.component';
import { Router } from '@angular/router';
import { element } from 'protractor';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.scss'],
  
})

export class VideosComponent implements OnInit {
  videos:Element[];
  @Input() tipo:string;

  constructor(private elementService:ElementUsuarioService, private dialog: MatDialog,
    private router: Router) 
  {
    this.videos= [];
    this.tipo="Videos";
   }
  
  ngOnInit(): void {
      this.obtenerElementos();
      this.tipo=this.router.url;
      const tag = document.createElement('script');
      tag.src = 'https://www.youtube.com/iframe_api/';
      document.body.appendChild(tag);
  }
  verVideos(p:Element)
  {
    
    const dialogRef = this.dialog.open(VerNoticiaComponent, {
      width: '800px',
      height:'500px',
      data:{}
    });
   dialogRef.componentInstance.noticia=p; 
    dialogRef.afterClosed().subscribe(result => {/* 
      console.log(`Dialog result: ${result}`); */
    });
  
  }
  obtenerElementos()
  {
    this.elementService.getElementos().subscribe(result=>{  
      result.forEach(e=>
        {
          if(('/'+e.tipo.toLowerCase())==this.router.url)
          {
            
            let elm= new Element();
            elm.titulo=e.titulo;
            elm.descripcion_larga=e.descripcion_larga;
            elm.fecha=e.fecha;
            // elm.imagen=e.imagen;
            elm.resumen=e.resumen;
            elm.tipo=e.tipo;          
            this.videos.push(elm);
          }
        }
       
        );
    });
  }
  ngDoCheck(): void {
    this.tipo=this.capitalize(this.router.url.slice(1));  
   }
   capitalize(word:string) {
    return word[0].toUpperCase() + word.slice(1);
  }
 
}
