import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Contacto } from '../../../../../Model/contacto.model';
import { ElementsAdminService } from '../../../Services/elements-admin.service';
import Swal from 'sweetalert2'
@Component({
  selector: 'app-editarCampos-admin',
  templateUrl: './editarCampos-admin.component.html',
  styleUrls: ['./editarCampos-admin.component.scss']
})
export class EditarCamposAdminComponent implements OnInit {
  contacto:Contacto;
  formulario:FormGroup;
  contactoRef:any;
  constructor(private elementService:ElementsAdminService, private formBuilder: FormBuilder) { 
    this.contacto= new Contacto();
  }

  ngOnInit() {
    this.crearFormulario();
    this.elementService.getContacto().subscribe(result=>{     
      result.forEach(c=>
        {
          console.log("CONTACTO RECIBIDO:   ",c);
            this.contactoRef=c;
            let cont= new Contacto();
            cont.celular=c.celular;
            cont.correo=c.correo;
            cont.direccion=c.direccion;
            cont.facebook=c.facebook;
            cont.twitter=c.twitter;
            cont.instagram=c.instagram;            
            this.contacto=cont;
            this.formulario.controls['correo'].setValue(cont.correo);
            this.formulario.controls['celular'].setValue(cont.celular);
            this.formulario.controls['direccion'].setValue(cont.direccion);
            this.formulario.controls['facebook'].setValue(cont.facebook);
            this.formulario.controls['twitter'].setValue(cont.twitter);
            this.formulario.controls['instagram'].setValue(cont.instagram);
        });        
      });
  }
  crearFormulario() {
    this.formulario = this.formBuilder.group(
      {
        correo: [this.contacto.correo, [Validators.required]],
        celular: [this.contacto.celular, [Validators.required]],
        direccion: [this.contacto.direccion, [Validators.required]],
        facebook: [this.contacto.facebook, [Validators.required]],
        twitter: [this.contacto.twitter, [Validators.required]],
        instagram: [this.contacto.instagram, [Validators.required]]
      });
    this.formulario.valueChanges.subscribe(
      value => {
        console.log("Asignando a correo: ",this.contacto.correo);
      }
    );
  }
  onSubmit()
  {
     let ct=new Contacto();
    ct.celular=this.formulario.get('celular').value;
    
    ct.correo=this.formulario.get('correo').value;
    
    ct.direccion=this.formulario.get('direccion').value;
    ct.facebook=this.formulario.get('facebook').value;
    ct.twitter=this.formulario.get('twitter').value;
    ct.instagram=this.formulario.get('instagram').value;
    console.log("### ELEMENTO A GUARDAR:   ",ct); 
    this.elementService.editContact(this.contactoRef,ct);
    this.okMessage();
  }
  okMessage()
  {
    Swal.fire({
      icon: 'success',
      title: 'Contacto Editado',
      text: 'El contacto fué editado Correctamente!',
    });
    this.ngOnInit();
  }

}
