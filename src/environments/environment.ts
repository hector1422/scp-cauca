// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDy5XQozuVP2-aGe1rdGTg7GHnArm5jVVc",
    authDomain: "scp-cauca.firebaseapp.com",
    databaseURL: "https://scp-cauca.firebaseio.com",
    projectId: "scp-cauca",
    storageBucket: "scp-cauca.appspot.com",
    messagingSenderId: "1052988844982",
    appId: "1:1052988844982:web:39dffe499f4171f5c32f17",
    measurementId: "G-3ZW3SBXPDG"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
