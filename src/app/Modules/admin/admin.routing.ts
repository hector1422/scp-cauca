import { Routes, RouterModule } from '@angular/router';
import { AdminFunctionsComponent } from './Components/admin-functions/admin-functions.component';
import { LoginAdminComponent } from './Components/login-admin/login-admin.component';
import {AuthGuard} from './auth.guard'
const routes: Routes = [
  {
    path: 'admin',
    component: AdminFunctionsComponent, canActivate: [AuthGuard]
  },
  {
    path: 'login',
    component: LoginAdminComponent,
  }
];

export const AdminRoutes = RouterModule.forChild(routes);
