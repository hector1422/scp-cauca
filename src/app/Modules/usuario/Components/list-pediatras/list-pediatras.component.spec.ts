import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPediatrasComponent } from './list-pediatras.component';

describe('ListPediatrasComponent', () => {
  let component: ListPediatrasComponent;
  let fixture: ComponentFixture<ListPediatrasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPediatrasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPediatrasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
