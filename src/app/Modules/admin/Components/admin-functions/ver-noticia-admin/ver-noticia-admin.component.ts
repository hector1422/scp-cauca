import { Element } from '../../../../../Model/element.model';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-ver-noticia-admin',
  templateUrl: './ver-noticia-admin.component.html',
  styleUrls: ['./ver-noticia-admin.component.scss']
})
export class VerNoticiaAdminComponent implements OnInit {

  noticia:Element;
  constructor(public dialogoReg:MatDialogRef<VerNoticiaAdminComponent>) 
  { 
    this.noticia=new Element();
  }

  ngOnInit() {
  }
  ok()
  {
    this.dialogoReg.close();
  }

}
