import { Pediatra } from './../../../../Model/pediatra.model';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
@Component({
  selector: 'app-ver-pediatra-usuario',
  templateUrl: './ver-pediatra-usuario.component.html',
  styleUrls: ['./ver-pediatra-usuario.component.scss']
})
export class VerPediatraUsuarioComponent implements OnInit {
  p:Pediatra;
  constructor(public dialogoReg:MatDialogRef<VerPediatraUsuarioComponent>) 
  {
    this.p=new Pediatra();
  }

  ngOnInit() {
  }
  ok()
  {
    this.dialogoReg.close();
  }
}
