import { Component, OnInit } from '@angular/core';
import {Element} from '../../../../Model/element.model'
import {ElementUsuarioService} from '../../Services/element-usuario.service'
import { MatDialog } from '@angular/material/dialog';
import { VerNoticiaComponent } from '../ver-noticia/ver-noticia.component';
import { Router } from '@angular/router';
@Component({
  selector: 'app-list-articulos',
  templateUrl: './list-articulos.component.html',
  styleUrls: ['./list-articulos.component.scss']
})
export class ListArticulosComponent implements OnInit {
  eventos:Element[];
  event:Element;
  not:any;
  vacio=true;
  seleccionado=false;
  tipo:string;
  constructor(private elementService:ElementUsuarioService, private dialog: MatDialog,
    private router: Router) { 
    this.eventos= [];
    this.event=new Element();
  }

  ngOnInit(): void {
    this.obtenerElementos();
    this.tipo=this.router.url;
  }
  obtenerElementos()
  {
    this.elementService.getElementos().subscribe(result=>{  
      result.forEach(e=>
        {
           if(('/'+e.tipo.toLowerCase()).slice(0,4)==this.router.url.slice(0,4))
          {
            
            let elm= new Element();
            elm.titulo=e.titulo;
            elm.descripcion_larga=e.descripcion_larga;
            elm.fecha=e.fecha;
            elm.imagen=e.imagen;
            elm.resumen=e.resumen;
            elm.tipo=e.tipo;           
           this.eventos.push(elm);
          }
        }
       
        );
    });
  }
ngDoCheck(): void {
 this.tipo=this.capitalize(this.router.url.slice(1));  
}
  verNoticia(n:Element)
  {
    const dialogRef = this.dialog.open(VerNoticiaComponent, {
      width: '800px',
      height:'500px',
      data:{}
    });
   dialogRef.componentInstance.noticia=n; 
    dialogRef.afterClosed().subscribe(result => {/* 
      console.log(`Dialog result: ${result}`); */
    });
  }
  capitalize(word:string) {
    return word[0].toUpperCase() + word.slice(1);
  }

}
